const Win = require('mongoose').model('Win');

/**
 * LIST
 * 
 * Retrieve ALL Wins from the database
 */


/**
 * CREATE
 * 
 * Create a new Win document
 */


/**
 * READ
 * 
 * Retrieve a SINGLE Win from the database
 */


/**
 * UPDATE
 * 
 * Update an existing Win
 */


/**
 * DELETE
 * 
 * Delete an existing Win
 */